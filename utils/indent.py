#!/usr/bin/env python3

# indent.py

# Take JSON output from curl and indent it to make it pretty:
#
# Read from stdin.
#
# Write to stdout.

import json
import sys

x = '#######################################################################'
print(x)
print('')

try:
    raw = sys.stdin.readlines()
    oneLine = ' '.join(raw)
    theStruct = json.loads(oneLine)
    print(json.dumps( theStruct, indent=4))
except:                                          # pylint: disable=bare-except
    print('*** Failed to read JSON data. ***')

print('')
print(x)

# The End.
