# Makefile for go-crudl-web-app-users-01

# Assume make is being run in the project's top-level directory:
PWD := $(shell pwd)

export GOPATH=${PWD}

GOBUILD := go install

SOURCES := src/crudl/crudl.go

OBJ     := bin/crudl

.PHONY: all clean

all: $(OBJ)

clean:
	rm -f $(OBJ)

$(OBJ): external $(SOURCES)
	cd $(GOPATH)/src/crudl ; $(GOBUILD)

# Must run this once to get the external package:
external:
	go get github.com/mattn/go-sqlite3
	go get github.com/gorilla/pat

# The End.
