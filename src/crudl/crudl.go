// src/crudl.go

// Simple CRUDL web application:

package main

import (
  "database/sql"
  "encoding/json"
  "fmt"
  "io/ioutil"
  "log"
  "net/http"
  "os"
  "strconv"
  "strings"
  "time"
  "github.com/gorilla/pat"
  _ "github.com/mattn/go-sqlite3"
)

//////////////////////////////////////////////////////////////////////////////

// Constants:

const listenPort = 11182 // chosen at random (head -c /dev/random | sum)

const dbFilename = "dbfiles/theCrudlDb.db"

const delimiter = "/"

// const pathApi = strings.Join( []string{ "", "api", "v1"}, delimiter)
const pathApi = "/api/v1"

//////////////////////////////////////////////////////////////////////////////

// Message printing utility functions:

// Set this to non-zero to enable debug printing:
// const enableDebugPrint = 0
const enableDebugPrint = 1

func printDebug(msg string) {
  if (0 == enableDebugPrint) {
    return
  }
  fmt.Printf( "DEBUG:     %s\n", msg)
}

func printInfo(msg string) {
  fmt.Printf( "     info: %s\n", msg)
}

func printWarning(msg string) {
  fmt.Printf( "  Warning: %s\n", msg)
}

// This function takes a string.
// This function does not return:
func printError(msg string) {
  fmt.Printf( "*** ERROR: %s\n", msg)
  os.Exit(1)
}

// This function takes an error.
// This function does not return:
func printFatal(err error) {
  log.Fatal(err)
  os.Exit(1)
}

// This function does not return:
func printUsageError(msg string) {
  fmt.Printf( "*** ERROR: %s\n\n", msg)
  printUsage()
  os.Exit(1)
}

// Print the usage message and return:
func printUsage() {
  fmt.Printf("usage: %s { 'old' | 'new' }\n", os.Args[0])
}


//////////////////////////////////////////////////////////////////////////////

// Return true iff the DB file exists:
func dbFileExists() bool {
  printDebug("dbFileExists starting:")
  _, err := os.Stat(dbFilename)
  result := (nil == err)
  printDebug(fmt.Sprintf("dbFileExists finished: %v", result))
  return(result)
}

//////////////////////////////////////////////////////////////////////////////

// Remove the file, unconditionally.  If error (including that the file does
// not exist), error out.
func dbFileRemoveUnconditionally() {
  printDebug("dbFileRemoveUnconditionally starting:")
  err := os.Remove(dbFilename)
  if (err != nil) {
    printFatal(err)
    // That function does not return.
  }
  printDebug("dbFileRemoveUnconditionally finished.")
}

//////////////////////////////////////////////////////////////////////////////

// Remove the file if it exists.
func dbFileGone() {
  printDebug("dbFileGone starting:")
  if (dbFileExists()) {
    dbFileRemoveUnconditionally()
  }
  if (dbFileExists()) {
    printError("Database file still exists.")
    // That function does no return.
  }
  printDebug("dbFileGone finished.")
}

//////////////////////////////////////////////////////////////////////////////

// Struct for initial pre-load and perhaps more.

type userRecord struct {
  email string
  first string
  last  string
  stamp string
}

//////////////////////////////////////////////////////////////////////////////

// Return a slice for pre-populating the DB.
//
// Attempted to make a constant, but array/slice contants are apparently not
// allowed.  There are likely more elegant ways to code this, but ...
//
// Values based loosely on https://www.imdb.com/title/tt0067992/fullcredits
// and https://en.wikipedia.org/wiki/Willy_Wonka_%26_the_Chocolate_Factory
func makePrePopData() [] userRecord {
  var result [] userRecord
  result = make( [] userRecord, 10)
  //
  result[0] = userRecord { "willy-wonka@chocolate-factory.com",
                           "Willy",
                           "Wonka",
                           "1940-12-25 00:00:00 +0000 UTC",
                         }
  result[1] = userRecord { "grandpa-joe@aol.com",
                           "Joe",
                           "Oldster",
                           "1911-01-02 03:04:05 +0000 UTC",
                         }
  result[2] = userRecord { "golden@ticket.com",
                           "Charlie",
                           "Bucket",
                           "1960-03-06 09:12:15 +0000 UTC",
                         }
  result[3] = userRecord { "boss@nut-business.com",
                           "Mister",
                           "Salt",
                           "1929-04-08 12:16:20 +0000 UTC",
                         }
  result[4] = userRecord { "spoiled@rotten.net",
                           "Veruca",
                           "Salt",
                           "1959-10-11 02:04:06 +0000 UTC",
                         }
  result[5] = userRecord { "buyer@gum.com",
                           "Mister",
                           "Beauregarde",
                           "1927-07-11 15:19:23 +0000 UTC",
                         }
  result[6] = userRecord { "chewer@gum.com",
                           "Violet",
                           "Beauregarde",
                           "1957-05-24 19:18:12 +0000 UTC",
                         }
  result[7] = userRecord { "enabler@nonstop.tv",
                           "Missus",
                           "Teevee",
                           "1930-06-22 21:20:19 +0000 UTC",
                         }
  result[8] = userRecord { "obsessed@nonstop.tv",
                           "Mike",
                           "Teevee",
                           "1960-02-11 02:03:04 +0000 UTC",
                         }
  result[9] = userRecord { "heavy-eater@glutton.us",
                           "Augustus",
                           "Gloop",
                           "1955-11-17 09:10:11 +0000 UTC",
                         }
  return(result)
}

//////////////////////////////////////////////////////////////////////////////

// Create the DB (unconditionally)
func createDb() {
  printDebug("createDb starting:")
  //
  db, err := sql.Open( "sqlite3", dbFilename)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  defer db.Close()
  //
  statement, err := db.Prepare(
      "CREATE TABLE users ( id INTEGER PRIMARY KEY, email TEXT" +
      ",  firstName  TEXT, lastName TEXT, timestamp DATE)")
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  //
  result, err := statement.Exec()
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  printDebug(fmt.Sprintf( "result: %V", result))
  //
  // Populate it with 10+ user records.
  printDebug("Will pre-populate the DB.")
  s := fmt.Sprintf( "INSERT INTO users %s VALUES %s",
                    "( email, firstName, lastName, timestamp)",
                    "( ?, ?, ?, ?)")
  statement, _ = db.Prepare(s)
  for _, v := range makePrePopData() {
    printDebug(fmt.Sprintf("pre-populating with %v", v))
    statement.Exec( v.email, v.first, v.last, v.stamp)
  }
  printDebug("createDb finished.")
}

//////////////////////////////////////////////////////////////////////////////

// Based on argument and possible pre-existing file, maybe create a new file.

func maybeCreateDb(arg string) {
  printDebug(fmt.Sprintf("maybeCreateDb starting: arg is %s", arg))
  exists := dbFileExists()
  if (("old" == arg) && exists) {
    // Do nothing; keep the old file.
    printDebug("maybeCreateDb doing nothing.")
    return
  } else {
    if (exists) {
      // Delete it, unconditionally.
      dbFileRemoveUnconditionally()
    }
    // Now, create the new file.
    createDb()
  }
  printDebug("maybeCreateDb finished.")
}

//////////////////////////////////////////////////////////////////////////////

// Get the argument.

func getArgs() string {
  printDebug("getArgs starting:")
  leng := len(os.Args)
  printDebug(fmt.Sprintf( "leng: %d", leng))
  if (2 < leng) {
    printUsageError("Too many arguments.")
    // That function does not return.
  }
  if (2 > leng) {
    printUsageError("Too few arguments.")
    // That function does not return.
  }
  arg := os.Args[1]
  printDebug(fmt.Sprintf( "arg: %s", arg))
  if (("old" != arg) && ("new" != arg)) {
    printUsageError(fmt.Sprintf( "Invalid argument: %s", arg))
    // That function does not return.
  }
  printDebug(fmt.Sprintf( "getArgs finished: %s", arg))
  return(arg)
}

//////////////////////////////////////////////////////////////////////////////

// Utility function to build a full route/link path from a tail:

func buildPath(args []string) string {
  printDebug(fmt.Sprintf( "buildPath starting: %s", args))
  result := strings.Join( args, delimiter)
  if ( ! strings.HasSuffix( result, "/")) {
    result += "/"
  }
  printDebug(fmt.Sprintf( "buildPath finished: %s", result))
  return(result)
}

// Utility function to open the DB:

func openDb() * sql.DB {
  printDebug("openDb starting:")
  db, err := sql.Open( "sqlite3", dbFilename)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  printDebug("openDb finished.")
  return(db)
}

// Utility function to set the response header to JSON and encode the result.

func writeResponse( w http.ResponseWriter, d interface{}) {
  printDebug("writeResponse starting:")
  w.Header().Set("Content-Type", "application/json")
  json.NewEncoder(w).Encode(d)
  printDebug("writeResponse finished.")
}

// Utility function to return a single user record from a list and to error
// out if there are multiple records.

func userList2single(l []map[string]interface{}) map[string]interface{} {
  printDebug("userList2single starting:")
  leng := len(l)
  if (1 < leng) {
    printError(fmt.Sprintf( "expected 1 user record, got %d", leng))
    // That function does not return.
  }
  var result map[string]interface{}
  if (1 == leng) {
    result = l[0]
  } else {
    // There might be a more elegant way to handle this, but ...
    result = nil
  }
  printDebug("userList2single finished.")
  return(result)
}

//////////////////////////////////////////////////////////////////////////////

// The DB access functions:

// Get a list of the IDs as ints:
func getUsersIdsOnly() [] int {
  printDebug("getUsersIdsOnly starting:")
  db := openDb()
  defer db.Close()
  //
  rows, err := db.Query("SELECT id FROM users")
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  result := [] int {}
  var thisId int
  for rows.Next() {
    rows.Scan(&thisId)
    result = append( result, thisId)
  }
  printDebug(fmt.Sprintf( "getUsersIdsOnly finished: %v", result))
  return(result)
}

// Shared function: Run GET user(s) query and return user record(s):
//
// If a single record is expected, there should be only one in the list.
//
func rows2UserRecords(rows * sql.Rows) []map[string]interface{} {
  printDebug("rows2UserRecords starting:")
  var result []map[string]interface{}
  var thisId        int
  var thisEmail     string
  var thisFirstName string
  var thisLastName  string
  var thisTimestamp string
  for rows.Next() {
    rows.Scan( &thisId, &thisEmail, &thisFirstName, &thisLastName,
               &thisTimestamp)
    element := make(map[string]interface{})
    element["id"]         = thisId
    element["email"]      = thisEmail
    element["first-name"] = thisFirstName
    element["last-name"]  = thisLastName
    element["timestamp"]  = thisTimestamp
    result = append( result, element)
  }
  printDebug(fmt.Sprintf( "rows2UserRecords finished: %d", len(result)))
  return(result)
}

// Get a list of full user records as a map with dynamic typed values:
func getUsersFull() []map[string]interface{} {
  printDebug("getUsersFull starting:")
  db := openDb()
  defer db.Close()
  //
  q := "SELECT id, email, firstName, lastName, timestamp FROM users"
  rows, err := db.Query(q)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  result := rows2UserRecords(rows)
  printDebug(fmt.Sprintf( "getUsersFull finished: %v", result))
  return(result)
}

// Get one specified full user record with dynamic typed values:
func getUserN(n int) map[string]interface{} {
  printDebug("getUserN starting:")
  db := openDb()
  defer db.Close()
  //
  q1 := "SELECT id, email, firstName, lastName, timestamp " +
        "FROM users WHERE id = ?"
  stmt, err := db.Prepare(q1)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  rows, err := stmt.Query(n)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  results := rows2UserRecords(rows)
  result := userList2single(results)
  printDebug(fmt.Sprintf( "getUserN finished: %v", result))
  return(result)
}

// Create a new user record:
func createUserRecord( email string,
                       firstName string,
                       lastName string) map[string]interface{} {
  printDebug("createUserRecord starting:")
  stamp := time.Now()
  db := openDb()
  defer db.Close()
  //
  s := fmt.Sprintf( "INSERT INTO users %s VALUES %s",
                    "( email, firstName, lastName, timestamp)",
                    "( ?, ?, ?, ?)")
  stmt, err := db.Prepare(s)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  // Neither .Exec() nor .Query() return anything useful.
  //
  // .Exec() returns type sql.driverResult, but I can't find it in the package
  // source or in any documentation.
  //
  _, err = stmt.Exec( email, firstName, lastName, stamp)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  //
  // Now, find the row (hopefully only one--see README.md tradeoffs) that was
  // just created.  The client needs to know the ID that was just created.
  //
  q1 := "SELECT id, email, firstName, lastName, timestamp " +
        " FROM users WHERE email = ? AND firstName = ? AND " +
        " lastName = ? AND timestamp = ?"
  stmt, err = db.Prepare(q1)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  rows, err := stmt.Query( email, firstName, lastName, stamp)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  results := rows2UserRecords(rows)
  result := userList2single(results)
  printDebug("createUserRecord finished.")
  return(result)
}

// Subordinate function to update one string field of the record with the
// specified ID:
func updateOneField( db * sql.DB, n int,
                     fieldName string, fieldValue string) {
  printDebug("updateOneField starting:")
  s := fmt.Sprintf( "UPDATE users SET %s = ? WHERE id = ?", fieldName)
  stmt, err := db.Prepare(s)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  _, err = stmt.Exec( fieldValue, n)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  printDebug("updateOneField finished.")
}

// Subordinate function to update the timestamp field of the record with the
// specified ID:
func updateTimestamp( db * sql.DB, n int, val time.Time) {
  printDebug("updateTimestamp starting:")
  s := "UPDATE users SET timeStamp = ? WHERE id = ?"
  stmt, err := db.Prepare(s)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  _, err = stmt.Exec( val, n)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  printDebug("updateTimestamp finished.")
}

// Update an existing user record:
//
// For each supplied field, empty string means no-op.
//
// Even if all supplied fields are empty, the timestamp still gets updated.
//
func updateUserRecord( n int, email string, firstName string,
                       lastName string) map[string]interface{} {
  printDebug("updateUserRecord starting:")
  stamp := time.Now()
  db := openDb()
  defer db.Close()
  //
  if ("" != email) {
    updateOneField( db, n, "email", email)
  }
  if ("" != firstName) {
    updateOneField( db, n, "firstName", firstName)
  }
  if ("" != lastName) {
    updateOneField( db, n, "lastName", lastName)
  }
  updateTimestamp( db, n, stamp)
  //
  // Finally, return the updated record.
  result := getUserN(n)
  printDebug("updateUserRecord finished.")
  return(result)
}

// Delete one specified full user record:
// Return the element _before_ it was deleted.
func deleteUserN(n int) map[string]interface{} {
  printDebug("deleteUserN starting:")
  db := openDb()
  defer db.Close()
  //
  // Before being destructive, capture what we are about to destroy.
  result := getUserN(n)
  //
  // If the element is nil or has no ID field, that means the operation
  // failed.
  if (nil == result) {
    // Give up; let the handler report bad status.
    return(result)
  }
  _, hasId := result["id"].(int)
  if ( ! hasId) {
    // Give up; let the handler report bad status.
    return(result)
  }
  //
  // Now, be destructive.
  q1 := "DELETE FROM users WHERE id = ?"
  stmt, err := db.Prepare(q1)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  _, err = stmt.Exec(n)
  if (nil != err) {
    printFatal(err)
    // That function does not return.
  }
  //
  // Return the result captured earlier.
  printDebug(fmt.Sprintf( "deleteUserN finished: %v", result))
  return(result)
}

//////////////////////////////////////////////////////////////////////////////

// The handler functions:

// Starting point for whole API:
func handleGetRoot( w http.ResponseWriter, r *http.Request) {
  printDebug("handleGetRoot starting:")
  rootStruct := map[string][]string{}
  pathUsers     := buildPath([]string{ pathApi, "users"})
  pathUsersFull := buildPath([]string{ pathApi, "users-full"})
  rootStruct["entities"] = []string{ pathUsers, pathUsersFull}
  writeResponse( w, rootStruct)
  printDebug("handleGetRoot finished.")
}

func handleGetUsers( w http.ResponseWriter, r *http.Request) {
  printDebug("handleGetUsers starting:")
  // The JSON and Go tutorial uses an empty interface type for dynamic types:
  // the ID is an int, and the link is a string.
  var elements []map[string]interface{}
  for _, id := range getUsersIdsOnly() {
    element := make(map[string]interface{})
    element["id"]   = id
    element["link"] = buildPath([]string{ pathApi, strconv.Itoa(id)})
    elements = append( elements, element)
  }
  writeResponse( w, elements)
  printDebug("handleGetUsers finished.")
}

func handlePostUsers( w http.ResponseWriter, r *http.Request) {
  printDebug("handlePostUsers starting:")
  theInput, err := ioutil.ReadAll(r.Body)
  if (nil != err) {
    http.Error( w, err.Error(), http.StatusBadRequest)
    return
  }
  printDebug(fmt.Sprintf( "theInput: %v", theInput))
  var theStruct map[string]interface{}
  json.Unmarshal( theInput, &theStruct)
  printDebug(fmt.Sprintf( "theStruct: %v", theStruct))
  //
  // Check for missing keys:
  email, has1 := theStruct["email"].(string)
  firstName, has2 := theStruct["first-name"].(string)
  lastName, has3 := theStruct["last-name"].(string)
  if ( ! (has1 && has2 && has3)) {
    http.Error( w, "Key(s) missing from POST data", http.StatusBadRequest)
    return
  }
  element := createUserRecord( email, firstName, lastName)
  // The element should not be nil.
  writeResponse( w, element)
  printDebug("handlePostUsers finished.")
}

func handleGetUsersFull( w http.ResponseWriter, r *http.Request) {
  printDebug("handleGetUsersFull starting:")
  var elements []map[string]interface{}
  for _, element := range getUsersFull() {
    var id int
    id = element["id"].(int)
    element["link"] = buildPath([]string{ pathApi, strconv.Itoa(id)})
    elements = append( elements, element)
  }
  writeResponse( w, elements)
  printDebug("handleGetUsersFull finished.")
}

func handleGetUsersN( w http.ResponseWriter, r *http.Request) {
  printDebug("handleGetUsersN starting:")
  nstr := r.URL.Query().Get(":id")
  n, err := strconv.Atoi(nstr)
  if (nil != err) {
    http.Error( w, err.Error(), http.StatusBadRequest)
    return
  }
  element := getUserN(n)
  // The element might be nil.  There might be a more elegant way to handle
  // this, but ... 
  if (nil != element) {
    // Add the link entry:
    element["link"] = buildPath([]string{ pathApi, strconv.Itoa(n)})
  }
  writeResponse( w, element)
  printDebug("handleGetUsersN finished.")
}

func handlePutUsersN( w http.ResponseWriter, r *http.Request) {
  printDebug("handlePutUsersN starting:")
  //
  nstr := r.URL.Query().Get(":id")
  n, err := strconv.Atoi(nstr)
  if (nil != err) {
    http.Error( w, err.Error(), http.StatusBadRequest)
    return
  }
  //
  theInput, err := ioutil.ReadAll(r.Body)
  if (nil != err) {
    http.Error( w, err.Error(), http.StatusBadRequest)
    return
  }
  printDebug(fmt.Sprintf( "theInput: %v", theInput))
  var theStruct map[string]interface{}
  json.Unmarshal( theInput, &theStruct)
  printDebug(fmt.Sprintf( "theStruct: %v", theStruct))
  // For this operation, missing keys translate to empty string.
  email, has1 := theStruct["email"].(string)
  if ( ! has1) {
    email = ""
  }
  firstName, has2 := theStruct["first-name"].(string)
  if ( ! has2) {
    firstName = ""
  }
  lastName, has3 := theStruct["last-name"].(string)
  if ( ! has3) {
    lastName = ""
  }
  //
  element := updateUserRecord( n, email, firstName, lastName)
  // If the element is nil or has no ID field, that means the operation
  // failed.
  if (nil == element) {
    http.Error( w, "Update record failed--nil result.",
                http.StatusBadRequest)
    return
  }
  _, hasId := element["id"].(int)
  if ( ! hasId) {
    http.Error( w, "Update record faile--no ID in result.",
                http.StatusBadRequest)
    return
  }
  // Add the link entry:
  element["link"] = buildPath([]string{ pathApi, strconv.Itoa(n)})
  writeResponse( w, element)
  printDebug("handlePutUsersN finished.")
}

func handleDeleteUsersN( w http.ResponseWriter, r *http.Request) {
  printDebug("handleDeleteUsersN starting:")
  nstr := r.URL.Query().Get(":id")
  n, err := strconv.Atoi(nstr)
  if (nil != err) {
    http.Error( w, err.Error(), http.StatusBadRequest)
    return
  }
  element := deleteUserN(n)
  //
  // If the element is nil or has no ID field, that means the operation
  // failed.
  if (nil == element) {
    http.Error( w, "Delete record failed--nil element.",
                http.StatusBadRequest)
    return
  }
  _, hasId := element["id"].(int)
  if ( ! hasId) {
    http.Error( w, "Delete record faile--no ID in element.",
                http.StatusBadRequest)
    return
  }
  // The element might be nil.  There might be a more elegant way to handle
  // this, but ... 
  if (nil != element) {
    // Add the link entry:
    element["link"] = buildPath([]string{ pathApi, strconv.Itoa(n)})
  }
  writeResponse( w, element)
  printDebug("handleDeleteUsersN finished.")
}

//////////////////////////////////////////////////////////////////////////////

// Create the Gorilla pattern mux and assign the handlers.
func buildMux() * pat.Router {
  printDebug("buildMux starting:")
  //
  theMux := pat.New()
  //
  // The literature says to assign the longer paths first:
  theMux.Get(    pathApi + "/users/{id}", handleGetUsersN)
  theMux.Put(    pathApi + "/users/{id}", handlePutUsersN)
  theMux.Delete( pathApi + "/users/{id}", handleDeleteUsersN)
  theMux.Get(    pathApi + "/users-full", handleGetUsersFull)
  theMux.Get(    pathApi + "/users",      handleGetUsers)
  theMux.Post(   pathApi + "/users",      handlePostUsers)
  theMux.Get(    pathApi,                 handleGetRoot)
  //
  printDebug("buildMux finished.")
  return(theMux)
}

//////////////////////////////////////////////////////////////////////////////

// Launch the server using DefaultServeMux:
func launchHttpServer(theMux * pat.Router) {
  thePort := listenPort
  printDebug(fmt.Sprintf("launchHttpServer starting on port %d", thePort))
  err := http.ListenAndServe( fmt.Sprintf( ":%d", thePort), theMux)
  if (nil != err) {
    printFatal(err)
  }
  printDebug("launchHttpServer finished.")
}

//////////////////////////////////////////////////////////////////////////////

func main() {
  printDebug("main starting:")
  arg := getArgs()
  printDebug(fmt.Sprintf( "arg: %s", arg))
  maybeCreateDb(arg)
  //
  theMux := buildMux()
  launchHttpServer(theMux)
  printDebug("main finished.")
}

// The End.
