# go-crudl-web-app-users-01

A simple CRUDL web app with JSON API for student records.

How to build the application:
=============================

    The 'Makefile' will fetch the two external libraries and compile the
    application with these two commands:

        make

How to run the application:
===========================

    With current working directory at the top-level directory of the
    repository:

        To make a fresh, new database:

            bin/crudl new

    or

        To continue with the old database:

            bin/crudl old

Example cURL command to create a user record:
=============================================

    Distilled from tests/11-post-users:

        curl --post301 --verbose localhost:11182/api/v1/users --data '{ "email": "somebody@else.com", "first-name": "Somebody", "last-name":  "Else" }'

Time spent:
===========

    This was my first use of net/http, the sqlite3 binding, and the Gorilla
    pattern MUX.  Much of the following time was in learning those libraries.

    Thursday,  4/29,  1:15pm -  3:00pm
    Thursday,  4/29,  3:30pm -  4:00pm
    Monday,    5/3,   1:05pm -  4:05pm
    Wednesday, 5/5,   9:05am - 10:10am
    Wednesday, 5/5,  10:40am - 11:40am
    Wednesday, 5/5,   1:00pm -  4:45pm
    Thursday,  5/6,   1:00pm -  3:30pm

    Total: 13 hours 35 minutes (if I calculated correctly)

Tradeoffs:
==========

More info on tradeoffs may be in the files in the 'doc' directory.

TRADEOFF: To keep the DB open between calls or to close the DB each time.
For high-performance production work, DB connections would need to be kept
open for performance reasons.  For this challenge, plan to close the DB after
each call to minimize risk of file corruption issues.

TRADEOFF:

    What format should date be in?

        Golang native format:

            2021-05-03 14:23:44 +0000 UTC

TRADEOFF: It would have been simpler for getUserN() to do the same query as
getUsersFull() and filter the result.  However, that would have been very
slow and I/O intensive for large databases.  A query that fetches a single
record to begin with is much faster and less expensive.

TRADEOFF: For the case of creating a new user record, I have seen use cases
for a REST API (the rack-scale resource composition trade show demos, for
example) where the client needs to get back information about the resource
that was just created.  However, I could not get the 'INSERT' query to return
anything about the new row that was created.  The best I could do is to do a
'SELECT' query for all the exact field values from the 'INSERT' query.  In a
production system, there is a _TINY_ chance that duplicate records could be
created with the same timestamp.  One possible solution for that would be to
add a random seed or hash that would identify (to cryptographic confidence) a
unique client operation.  Out of time pressure, I did not implement that for
this coding challenge.

# The End.
